﻿using SITER_Users.Entity;
using static System.Reflection.Metadata.BlobBuilder;

namespace SITER_Users
{
    public class Dataseed
    {
        public static List<Users> GetUsers()
        {
            List<Users> users = new()
            {
                new Users()
                {
                    Id = 1,
                    Name="Vivek M S",
                    Email="viveksapate@gmail.com",
                    Age=27,
                    Gender="Male",
                    
                },

                new Users()
                {
                    Id = 2,
                    Name="Saju M",
                    Email="Saju@gmail.com",
                    Age=35,
                    Gender="Male",

                },

               new Users()
                {
                    Id = 3,
                    Name=" Sruthy ",
                    Email="Sruthy@gmai.com",
                    Age=27,
                    Gender="Female",

                },

            };
            return users;
        }
    }
}
