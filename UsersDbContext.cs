﻿using Microsoft.EntityFrameworkCore;
using SITER_Users.Entity;
using static System.Reflection.Metadata.BlobBuilder;

namespace SITER_Users
{
    public class UsersDbContext : DbContext
    {
        public UsersDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var connectionString = configuration.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // TODO: add data from SeedData
            modelBuilder.Entity<Users>().HasData(Dataseed.GetUsers());
        }
    }
}
